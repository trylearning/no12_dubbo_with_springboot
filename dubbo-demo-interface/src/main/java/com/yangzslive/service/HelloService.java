package com.yangzslive.service;

/**
 * interface层的接口
 */
public interface HelloService {

    String sayHello(String name);
}
