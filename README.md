### 说明

系统项目：

- interface 使用maven创建管理的普通Java项目，其中定义了对外提供的interface的服务接口定义
- provider 使用SpringBoot创建的Java类型项目，其中定义了对interface接口的实现，可以通过内置Tomcat容器运行，配置文件当中定义了provider启动的项目端口，zk的连接地址，在POM包当中主要是引入了web开发相关的jar包，interface接口的jar包，Dubbo相关的依赖jar包和zookeeper相关的依赖
- consumer 使用SpringBoot创建的Java类型项目，可以通过内置Tomcat容器运行。POM文件当中首先引入了web开发的相关jar包，interface接口的jar包，Dubbo相关的依赖的jar包和zookeeper相关的jar包；配置文件当中首先是定义了consumer项目启动的端口，zk的连接地址。

#### 注意

因为provider和consumer项目都是利用SpringBoot框架构建，所以对于dubbo的配置信息的加载是通过在主启动类上面添加@enableDubboAutoConfig注解来实现的。



#### 参考

https://www.javazhiyin.com/25919.html