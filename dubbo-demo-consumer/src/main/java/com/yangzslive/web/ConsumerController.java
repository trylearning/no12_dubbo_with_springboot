package com.yangzslive.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.yangzslive.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhensheng.yang
 * @since 2019/8/15
 **/
@RestController
public class ConsumerController {

    @Reference
    HelloService helloService;

    @RequestMapping("/hello")
    public String hello() {
        String hello = helloService.sayHello("world");
        System.out.println(helloService.sayHello("SnailClimb"));
        return hello;
    }
}
