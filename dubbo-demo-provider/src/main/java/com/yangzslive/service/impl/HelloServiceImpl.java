package com.yangzslive.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.yangzslive.service.HelloService;
import org.springframework.stereotype.Component;

/**
 * @author zhensheng.yang
 * @since 2019/8/15
 **/
@Component
@Service    //此处使用的是Dubbo的@Service的注解，而不是Spring的需要注意
public class HelloServiceImpl implements HelloService {
    @Override
    public String sayHello(String name) {
        return "Hello " + name;
    }
}
